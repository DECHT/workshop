FROM ubuntu:22.04

EXPOSE 80

RUN apt update
RUN apt -y upgrade
RUN apt install -y apache2
RUN echo "ESE 2023" > /var/www/html/index.html 

CMD ["apachectl", "-D", "FOREGROUND"]
